#include "rhio-zs05.h"

#include <Arduino.h>
#include <Wire.h>

#include "rhio-pinmap.h"
#define MON RH_UART_DEBUG

ZS05::ZS05() { _f = NULL; }

void ZS05::begin(int clockFrecuency) {
  _f = clockFrecuency;
  Wire.begin();
  Wire.setClock(_f);
  Wire.beginTransmission(0x5C);
  MON.println("Init ZS05 sensor reading.");
}

bool ZS05::checkSum() {
  int checkSum = 0;

  for (int i = 0; i < 4; i++) {
    checkSum = checkSum + measures[i];
  }
  if (checkSum == measures[4]) {
    MON.print("bien!");
    return true;
  }
  return false;
}
void ZS05::readTemperature() {
  Wire.beginTransmission(0x5C);
  Wire.write(byte(0x02));
  byte busStatus = Wire.endTransmission();
  if (busStatus != 0) {
    MON.print("I2C Bus communication problem...!");
    return;
  }
  delay(1000);
  Wire.requestFrom(0x5C, 2);
  int _temperature;
  for (byte i = 0; i < 2; i++) {
    if (i == 0) {
      _temperature = Wire.read() * 10;
    } else
      _temperature = Wire.read() + _temperature;

    temperature = (float)_temperature / (float)10;
  }
  MON.print("temperature:");
  MON.println(temperature);
  delay(50);
}
void ZS05::readHumidity() {
  Wire.beginTransmission(0x5C);
  Wire.write(byte(0x00));
  byte busStatus = Wire.endTransmission();
  if (busStatus != 0) {
    MON.print("I2C Bus communication problem...!");
    return;
  }
  delay(1000);
  Wire.requestFrom(0x5C, 2);
  int _humidity;
  for (byte i = 0; i < 2; i++) {
    if (i == 0) {
      _humidity = Wire.read() * 10;
    } else
      _humidity = Wire.read() + _humidity;
  }
  humidity = (float)_humidity / (float)10;
  MON.print("humidity:");
  MON.println(humidity);
  delay(50);
}
bool ZS05::readSensors() {
  Wire.beginTransmission(0x5C);
  Wire.write(byte(0x00));
  byte busStatus = Wire.endTransmission();
  if (busStatus != 0) {
    MON.print("I2C Bus communication problem...!");
    return false;
  }
  delay(1000);
  int _humidity, temperature;

  Wire.requestFrom(0x5C, 5);
  for (byte i = 0; i < 5; i++) {
    measures[i] = Wire.read();
    delay(50);
  }
  return checkSum();
}

float ZS05::getTemperature() {
  temperature = ((float)(measures[2] * 10 + measures[3]) / (float)10);
  return temperature;
}
float ZS05::getHumidity() {
  humidity = ((float)(measures[0] * 10 + measures[1]) / (float)10);
  return humidity;
}
void ZS05::getRaw() {
  Wire.beginTransmission(0x5C);
  Wire.write((byte)0x00);  // command byte
  byte busStatus = Wire.endTransmission();
  if (busStatus != 0) {
    MON.print("I2C Bus communication problem...!");
    while (1)
      ;  // wait for ever
  } else {
    MON.println("I2C Bus OK.");
    Wire.requestFrom(0x5C, 5);  // requesting 8-byte data from Slave
    while (Wire.available()) {
      MON.println(Wire.read());
    }
  }
}