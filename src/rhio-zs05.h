/*
    ZS05.h - This library allows you to read the ZS05 (temperature and
    humidity) Sensor.
   Created by Raquel Picazo february 14, 2022.
*/
#ifndef ZS05_h
#define ZS05_h

#include <Arduino.h>
class ZS05 {
 public:
  ZS05();

  void begin(int clockFrecuency);
  /* Public variables */
  void readTemperature();
  void readHumidity();
  bool readSensors();
  bool checkSum();
  float getHumidity();
  float getTemperature();
  void getRaw();

 private:
  /* Private variables */
  int _f;
  float temperature;
  float humidity;
  int measures[4];
};
#endif
