#include <Arduino.h>

#include "rhio-pinmap.h"
#include "rhio-zs05.h"

#define MON RH_UART_DEBUG
ZS05 zs05;
void setup() {
  // put your setup code here, to run once:
  MON.begin(9600);
  zs05.begin(20000);
  while (!MON) {
    ;
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  MON.println("INIT ZS05 Sensor Module");
  // First read the sensors and then take the temperature and humidity values
  // this way checks the checkSum value to ensure the good communication.
  if (zs05.readSensors()) {
    MON.print("Temperature");
    MON.println(zs05.getTemperature());
    MON.print("Humidity");
    MON.println(zs05.getHumidity());
  }
  delay(3000);

  // get all the raw values
  zs05.getRaw();
  // get temperature and humidity separated and without checksum check
  zs05.readTemperature();
  zs05.readHumidity();
  delay(3000);
}